#!/bin/bash
#SBATCH --ntasks=40             
#SBATCH --nodes=1   
#SBATCH --partition=smallmem

module load R/3.5.0
module load blast+
module load mcl
module load fastme
module load fasttree/2.1.8
module load openmpi/1.6.5

snakemake --configfile config.yaml --cores 40 -s Snakefile.py 

  
